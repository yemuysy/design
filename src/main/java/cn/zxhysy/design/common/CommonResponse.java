package cn.zxhysy.design.common;

import lombok.Data;

@Data
public class CommonResponse<T> {

    private boolean success;

    private String errCode;

    private String errMsg;

    private T data;

}
