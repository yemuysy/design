package cn.zxhysy.design.common;

import lombok.Data;

@Data
public class CommonPairResponse<L, R> {

    private boolean success;

    private L left;

    private R right;

    private String errCode;

    private String errMsg;

    public static <L, R> CommonPairResponse<L, R> success(L l, R r) {
        CommonPairResponse<L, R> commonPairResponse = new CommonPairResponse<>();
        commonPairResponse.setSuccess(true);
        commonPairResponse.setLeft(l);
        commonPairResponse.setRight(r);
        return commonPairResponse;
    }

    public static <L, R> CommonPairResponse<L, R> failure(String msg) {
        CommonPairResponse<L, R> commonPairResponse = new CommonPairResponse<>();
        commonPairResponse.setSuccess(false);
        commonPairResponse.setErrCode("142578");
        commonPairResponse.setErrMsg(msg);
        return commonPairResponse;
    }

}
