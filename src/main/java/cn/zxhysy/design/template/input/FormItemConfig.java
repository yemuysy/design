package cn.zxhysy.design.template.input;

import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import lombok.Data;

/**
 * 表单项配置
 */
@Data
public class FormItemConfig {

    /**
     * 备注
     */
    private String placeholder;

    /**
     * 标题
     */
    private String title;

    /**
     * 属性
     */
    private String code;

    /**
     * 组件
     */
    private String component;

    /**
     * 是否只读
     */
    private boolean readOnly;

    /**
     * 是否必填
     */
    private boolean required;

    /**
     * 是否多选
     */
    private boolean multiple;

    /**
     * 最小长度
     */
    private Integer minLength;

    /**
     * 最大长度
     */
    private Integer maxLength;

    /**
     * 表单类型
     */
    private FormItemTypeEnum type;

    /**
     * 最小正整数
     */
    private Integer minNumber;

    /**
     * 最大正整数
     */
    private Integer maxNumber;
}
