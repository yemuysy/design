package cn.zxhysy.design.template.output;

import lombok.Data;

/**
 * 组件属性
 */
@Data
public class FormComponentProps {

    /**
     * 是否只读
     */
    private boolean readOnly;

    /**
     * 备注
     */
    private String placeholder;

    /**
     * 自动宽度
     */
    private boolean autoWidth;

    /**
     * 多
     */
    private String multiple;

    /**
     * 模式
     */
    private String mode;
}
