package cn.zxhysy.design.template.output;

import lombok.Data;

import java.util.List;

/**
 * 表单项
 */
@Data
public class FormItem {

    /**
     * 标题
     */
    private String title;

    /**
     * 属性code
     */
    private String code;

    /**
     * 组件
     */
    private String component;

    /**
     * 组件属性
     */
    private FormComponentProps componentProps;

    /**
     * 约束规则
     */
    private List<FormItemRule> rules;

    /**
     * 设置是否模糊
     */
    private boolean fuzzySearch;
}
