package cn.zxhysy.design.template.output;

import lombok.Data;

/**
 * 表单项规则
 */
@Data
public class FormItemRule {

    /**
     * 是否必须
     */
    private boolean required;

    /**
     * 校验失败时消息
     */
    private String message;

    /**
     * 最小长度 例如：请输入最少对少个字节
     */
    private Integer minLength;

    /**
     * 最大长度
     */
    private Integer maxLength;

    /**
     * 最小数值
     */
    private Integer minimum;

    /**
     * 最大数值
     */
    private Integer maximum;
}
