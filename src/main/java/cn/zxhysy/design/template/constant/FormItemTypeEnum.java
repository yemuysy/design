package cn.zxhysy.design.template.constant;

/**
 * 表单项类型
 */
public enum FormItemTypeEnum {

    /**
     * 下拉框
     */
    DROPDOWN_SELECT,
    /**
     * 模糊查询
     */
    FUZZY_SEARCH,
    /**
     * 单行文本
     */
    TEXT_INPUT,
    /**
     * 多行文本
     */
    TEXT_AREA,
    /**
     * 数字选择框
     */
    NUMBER_PICKER

}
