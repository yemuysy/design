package cn.zxhysy.design.template.util;

import cn.zxhysy.design.template.input.FormItemConfig;
import cn.zxhysy.design.template.output.FormItem;

import java.util.List;

/**
 * 表单项管理服务
 */
public interface FormItemManager {

    /**
     * 将输入的表单 转换成表单项
     *
     * @param inputConfigs 输入的配置
     * @return 表单项列表
     */
    List<FormItem> convertFormItems(List<FormItemConfig> inputConfigs);

}
