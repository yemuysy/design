package cn.zxhysy.design.template.util;

import cn.zxhysy.design.factory.method.factory.StrategyFactory;
import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import cn.zxhysy.design.template.converter.FormItemConverter;
import cn.zxhysy.design.template.factory.FormItemConverterFactory;
import cn.zxhysy.design.template.output.FormItem;
import cn.zxhysy.design.template.input.FormItemConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 表单项管理服务
 */
@Component
public class FormItemManagerImpl implements FormItemManager {

    /**
     * 更改为工厂方法的工厂引入
     * //    @Autowired
     * //    private FormItemConverterFactory converterFactory;
     */
    @Autowired
    private StrategyFactory<FormItemTypeEnum, FormItemConverter> converterFactory;

    /**
     * 将输入的表单转换成表单项列表
     *
     * @param inputConfigs 表单配置
     * @return 表单项列表
     */
    @Override
    public List<FormItem> convertFormItems(List<FormItemConfig> inputConfigs) {
        return inputConfigs.stream()
                .map(this::convertFormItem)
                .collect(Collectors.toList());
    }

    /**
     * 表单项转换
     *
     * @param itemConfig 配置项配置
     * @return 表单
     */
    private FormItem convertFormItem(FormItemConfig itemConfig) {
        FormItemConverter converter = converterFactory.getStrategy(itemConfig.getType());

        if (converter == null) {
            throw new IllegalArgumentException("不存在转换器：" + itemConfig.getType());
        }

        return converter.convert(itemConfig);
    }
}
