package cn.zxhysy.design.template.converter;

import cn.zxhysy.design.template.output.FormComponentProps;
import cn.zxhysy.design.template.input.FormItemConfig;
import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import org.springframework.stereotype.Component;

/**
 * 下拉选择框的转换器
 */
@Component
public class DropdownSelectConverter extends FormItemConverter {

    @Override
    public FormItemTypeEnum getType() {
        return FormItemTypeEnum.DROPDOWN_SELECT;
    }

    @Override
    protected void afterPropsCreate(FormComponentProps props, FormItemConfig config) {
        props.setAutoWidth(false);

        if (config.isMultiple()) {
            props.setMode("multiple");
        }
    }
}
