package cn.zxhysy.design.template.converter;

import cn.zxhysy.design.template.input.FormItemConfig;
import cn.zxhysy.design.template.output.FormItemRule;
import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 数字选择器转换器
 */
@Component
public class NumberPickerConverter extends FormItemConverter {

    @Override
    public FormItemTypeEnum getType() {
        return FormItemTypeEnum.NUMBER_PICKER;
    }

    @Override
    protected void afterRulesCreate(List<FormItemRule> rules, FormItemConfig config) {
        Integer minNumber = config.getMinNumber();
        // 处理最小值
        if (minNumber != null) {
            FormItemRule minNumRule = new FormItemRule();

            minNumRule.setMinimum(minNumber);
            minNumRule.setMessage("输入数字不能小于 " + minNumber);

            rules.add(minNumRule);
        }

        Integer maxNumber = config.getMaxNumber();
        // 处理最大值
        if (maxNumber != null) {
            FormItemRule maxNumRule = new FormItemRule();

            maxNumRule.setMaximum(maxNumber);
            maxNumRule.setMessage("输入数字不能大于 " + maxNumber);

            rules.add(maxNumRule);
        }
    }
}
