package cn.zxhysy.design.template.converter;

import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import org.springframework.stereotype.Component;

/**
 * 单行文本框的转换器
 */
@Component
public class TextInputConverter extends CommonTextConverter {

    @Override
    public FormItemTypeEnum getType() {
        return FormItemTypeEnum.TEXT_INPUT;
    }
}
