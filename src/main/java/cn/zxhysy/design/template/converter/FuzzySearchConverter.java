package cn.zxhysy.design.template.converter;


import cn.zxhysy.design.template.output.FormComponentProps;
import cn.zxhysy.design.template.output.FormItem;
import cn.zxhysy.design.template.input.FormItemConfig;
import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import org.springframework.stereotype.Component;

/**
 * 模糊搜索框的转换器
 */
@Component
public class FuzzySearchConverter extends FormItemConverter {

    @Override
    public FormItemTypeEnum getType() {
        return FormItemTypeEnum.FUZZY_SEARCH;
    }

    @Override
    protected void afterItemCreate(FormItem item, FormItemConfig config) {
        item.setFuzzySearch(true);
    }

    @Override
    protected void afterPropsCreate(FormComponentProps props, FormItemConfig config) {
        props.setAutoWidth(false);
    }
}
