package cn.zxhysy.design.template.converter;

import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import org.springframework.stereotype.Component;

/**
 * 多行文本框的转换器
 */
@Component
public class TextAreaConvertor extends FormItemConverter {

    @Override
    public FormItemTypeEnum getType() {
        return FormItemTypeEnum.TEXT_AREA;
    }
}