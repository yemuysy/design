package cn.zxhysy.design.strategy.service;

import cn.zxhysy.design.common.CommonPairResponse;
import cn.zxhysy.design.strategy.obj.FormSubmitRequest;
import org.springframework.lang.NonNull;

import java.io.Serializable;

public interface FormService {

    CommonPairResponse<String, Serializable> submitForm(@NonNull FormSubmitRequest request);

}