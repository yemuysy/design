package cn.zxhysy.design.strategy.handler;


import cn.zxhysy.design.common.CommonPairResponse;
import cn.zxhysy.design.strategy.obj.FormSubmitRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class FormHsfSubmitHandler implements FormSubmitHandler<Serializable> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getSubmitType() {
        return "hsf";
    }

    @Override
    public CommonPairResponse<String, Serializable> handleSubmit(FormSubmitRequest request) {
        logger.info("HSF 模式提交：userId={}, formInput={}", request.getUserId(), request.getFormInput());

        // 进行 HSF 泛化调用，获得业务方返回的提示信息和业务数据
        CommonPairResponse<String, Serializable> response = hsfSubmitData(request);
        return response;
    }

    private CommonPairResponse<String, Serializable> hsfSubmitData(FormSubmitRequest request) {
        // 创建模型的逻辑
        return CommonPairResponse.success("HSF模式提交数据成功！", null);
    }
}
