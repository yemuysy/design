package cn.zxhysy.design.pipeline.handler.common;

import cn.hutool.json.JSONUtil;
import cn.zxhysy.design.pipeline.handler.ContextHandler;
import cn.zxhysy.design.pipeline.context.PipelineContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CommonHeadHandler implements ContextHandler<PipelineContext> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean handle(PipelineContext context) {
        logger.info("管道开始执行：context={}", JSONUtil.toJsonStr(context));

        // 设置开始时间
        context.setStartTime(LocalDateTime.now());

        return true;
    }
}