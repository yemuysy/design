``` 
导读：代理模式（Proxy Pattern）即为某一个对象提供一个代理对象，
由代理对象来接管被代理对象的各个方法的访问。

如果想为对象的某些方法做方法逻辑之外的附属功能（例如 打印出入参、处理异常、校验权限），
但是又不想（或是无法）将这些功能的代码写到原有方法中，那么可以使用代理模式。
```