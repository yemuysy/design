package cn.zxhysy.design.factory.method.configura;

import cn.zxhysy.design.factory.method.factory.StrategyFactory;
import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import cn.zxhysy.design.template.converter.FormItemConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置策略工厂，通过组合方式来减少继承方式带来的类增加
 */
@Configuration
public class FactoryConfig {

    @Bean
    public StrategyFactory<FormItemTypeEnum, FormItemConverter> formItemConverterFactory() {
        return new StrategyFactory<>(FormItemConverter.class);
    }

}
