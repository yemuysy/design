package cn.zxhysy.design.factory.method.strategy;

/**
 * 抽象的产品（策略）
 *
 * @param <T>
 */
@FunctionalInterface
public interface Strategy<T> {

    /**
     * 获得策略的标识
     */
    T getId();
}
