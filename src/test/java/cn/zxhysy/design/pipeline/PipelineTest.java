package cn.zxhysy.design.pipeline;

import cn.hutool.core.map.MapUtil;
import cn.zxhysy.design.pipeline.context.InstanceBuildContext;
import cn.zxhysy.design.pipeline.executor.PipelineExecutor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
public class PipelineTest {

    @Autowired
    private PipelineExecutor pipelineExecutor;

    @Test
    public void test(){
        InstanceBuildContext pipelineContext = new InstanceBuildContext();
        pipelineContext.setUserId(1L);
        pipelineContext.setFormInput( MapUtil.builder(new HashMap<String, Object>()).put("instanceName", "a").build());
        pipelineExecutor.acceptSync(pipelineContext);
    }

}
