package cn.zxhysy.design.proxy;

import cn.zxhysy.design.proxy.anno.ExceptionHandleAnno;
import cn.zxhysy.design.proxy.anno.InvokeRecordAnno;
import org.springframework.stereotype.Component;

@Component
public class BasicServiceImpl implements BasicService{

    @InvokeRecordAnno
    @ExceptionHandleAnno
    @Override
    public void test() {
        System.out.println("test -");
    }
}
