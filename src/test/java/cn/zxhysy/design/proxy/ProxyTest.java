package cn.zxhysy.design.proxy;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProxyTest {

    @Autowired
    private BasicService basicService;

    @Test
    public void test(){
        basicService.test();
    }

}
