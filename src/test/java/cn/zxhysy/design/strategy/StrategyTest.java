package cn.zxhysy.design.strategy;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.zxhysy.design.strategy.obj.FormSubmitRequest;
import cn.zxhysy.design.strategy.service.FormService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StrategyTest {

    @Autowired
    private FormService formService;

    @Test
    public void test() {
        String formInputJson = "{}";
        String submitType = "model";
        JSONObject formInput = JSONUtil.parseObj(formInputJson);

        FormSubmitRequest request = new FormSubmitRequest();
        request.setUserId(123456L);
        request.setSubmitType(submitType);
        request.setFormInput(formInput);

        formService.submitForm(request);
    }

}
