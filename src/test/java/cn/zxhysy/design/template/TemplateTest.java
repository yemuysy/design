package cn.zxhysy.design.template;

import cn.zxhysy.design.template.constant.FormItemTypeEnum;
import cn.zxhysy.design.template.input.FormItemConfig;
import cn.zxhysy.design.template.output.FormItem;
import cn.zxhysy.design.template.util.FormItemManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class TemplateTest {

    @Autowired
    private FormItemManager formItemManager;

    @Test
    public void test(){
        List<FormItemConfig> configList = new ArrayList<>();
        FormItemConfig inputItemConfig = new FormItemConfig();
        inputItemConfig.setTitle("姓名");
        inputItemConfig.setCode("name");
        inputItemConfig.setComponent("el-input");
        inputItemConfig.setPlaceholder("请输入姓名");
        inputItemConfig.setMinLength(2);
        inputItemConfig.setRequired(true);
        inputItemConfig.setReadOnly(false);
        inputItemConfig.setMultiple(false);
        inputItemConfig.setType(FormItemTypeEnum.TEXT_INPUT);

        configList.add(inputItemConfig);
        List<FormItem> formItems = formItemManager.convertFormItems(configList);

        System.out.println(formItems);
    }

}
